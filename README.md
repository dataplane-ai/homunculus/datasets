# Datasets for the Homunculus Applications

- **`kdd-synthetic-traces`** 
  - adapted from https://www.unb.ca/cic/datasets/nsl.html

- **`flowlens-flowmarkers-traces`** 
  - adapted from https://www.ndss-symposium.org/ndss-paper/flowlens-enabling-efficient-flow-classification-for-ml-based-network-security-applications/

- **`tmc-iot-traces`** 
  - adapted from https://iotanalytics.unsw.edu.au/iottraces.html
